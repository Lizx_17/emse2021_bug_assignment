\begin{figure}
\centering
\includegraphics[width=0.8\textwidth]{figure/lifecycle.pdf}\vspace*{-3ex}
\caption{The lifecycle of bug reports}
\label{fig: lifecycle}
\end{figure}
\section{Background}
\label{sec:background}
This section first introduces the bug triage in practice (Section~\ref{sec:background:triage}), and the prior assignment approaches (Section~\ref{sec:background:state}).


\subsection{Bug Report Triage in Practice}
\label{sec:background:triage}
\begin{figure}
	\centering
	\includegraphics[width= 0.8\textwidth]{figure/HBASE861.pdf}
	\caption{A piece of bug report in JIRA}
	\label{fig: sample}
\end{figure}




An issue tracking system~\cite{jeong2009improving} is a platform for tracking development issues, and most Apache projects use Jira~\cite{apache.jira} to track their issues. Figure \ref{fig: lifecycle} shows the lifecycle of handling a bug report. When a user reports a bug, a piece of report is created and its status is set to \textit{open}. Then, if a developer determines that the bug report is invalid, and it can be marked as \emph{invalid}, \emph{duplicate}, \emph{won't fix}, \emph{implemented}, \emph{not a bug}, \emph{cannot reproduce}, or \emph{incomplete} for different reasons. Here, as reported by Anvik et al.~\cite{anvik2006should} and Jeong et al.~\cite{jeong2009improving}, invalid bug reports can be marked as \emph{closed}. Next, developers will assign valid bug reports to experts. To distinguish an expert from other developers, we call an expert as \textit{assignee} in this paper. During the repairing process of a bug report, more developers can discuss its buggy behavior and review its patches. In their discussion, the values of some fields, like \textit{Priority} and \textit{Status}, can be changed, source files can be attached and relations to other issues can also be found. Finally, the \textit{resolution} and \textit{status} of a valid bug report are marked as \emph{fixed} and \emph{closed} after its fixed code passes all test cases. Sometimes, a few closed reports can be reopened if some problems still exist.



%\subsection{The Bug Reports of Open Source Software}
%\label{sec:background:state}
%\hao{In Figure \ref{fig: sample}, please choose another example, where its reporter and assignee are different! Please rewrite the following paragraphs, in a way that you do not have to read the figure to understand the texts.}

Figure \ref{fig: sample} shows the fragment of a bug report (\textit{\#HBASE-861}\cite{apache.hbase}), where it presents the details (\emph{e.g.}, its status) and the description of the bug.
This bug report is assigned and reassigned among three assignees, and discussed by two other developers. It is reopened once, and finally it is resolved. Here is its lifecycle:

\textit{\textbf{Creation.}} On \textit{02/Sep/08 20:55}, \textit{Jim Kellerman} reported that he obtained a wrong timestamp, and the bug report was then created. The bug report has a description: ``When an explicit timestamp is specified, no results should be returned if there is no value stored at that timestamp.'' Besides the description, the bug report attaches a code sample to illustrate how to trigger the bug.

\textit{\textbf{Discussion \& Tossing.}}
After its creation, three developers, \textit{stack}, \textit{Jim Kellerman} and \textit{Andrew Purtell}, discussed how to repair the bug. The bug report was initially assigned to \textit{Izaak Rubin}, but \textit{Jonathan Gray} pointed out that this bug is a part of a previous bug, which was fixed by \textit{stack}. As a result, the bug report was reassigned to \textit{stack} and was marked as resolved later.

\textit{\textbf{Reopen \& Fixed.}}
Only about 2 hours later, \textit{stack} realized that the bug was not fully fixed, and changed the bug report to reopened. After three days, the bug was fully fixed by \textit{Jonathan Gray}, and marked as closed.



\subsection{Bug Report Triage in the Literature}
\label{sec:background:state}

%In recent ten years, researchers have proposed various approaches to assign bug reports. In the literature, the problem of assigning bug reports is typically reduced to a classification problem, and researchers have used various techniques to handle the problem. For example, Cubranic and Murphy~\cite{murphy2004automatic} use Naive Bayes (NB), Anvik \emph{et al.}~\cite{anvik2006should,anvik2006automating} use Support Vector Machine (SVM), C4.5, and Expectation Maximization (EM)~\cite{anvik2007assisting}.
%Lin \emph{et al.}~\cite{lin2009empirical} also leverage SVM and C4.5, considering both textual and non-textual information. Jonsson et al.~\cite{jonsson2016automated} propose an ensemble model, which combines several known approaches. The above approaches extract features from bug reports, and use assigned programmers as labels of their classification models. These approaches ignore the expertise of programmers nor their relationships. To handle the problem, Information Retrieval (IR) based models and recommendation-based models are proposed to match the expertises of developers and the requirements of a report. Jeong \emph{et al.}~\cite{jeong2009improving} used Markov Chain to construct tossing graphs to depict the relationships of developers with reassignments considered. Based on tossing graphs, Bhattacharya \emph{et al.}~\cite{bhattacharya2010fine,bhattacharya2012automated} built tossing graphs to denote the reassignments of bug reports, and introduced an ablative analysis on such graphs to improve bug triage.
%Naguib \emph{et al.}~\cite{naguib2013bug} made recommendations by matching the topics of reports to the expertise of developers mined from activities profiles. Information Retrieval based models, (\emph{e.g.}, Kagdi \emph{et al}~\cite{kagdi2012assigning}) calculated the similarity of the newly reported bug with historical reports with source code entities and then recommended competent developers to solve it.
%
%In our study, we focus on the research line that reduces the bug assignment to a classification problem, since it is intensively studied and a recent approach~\cite{jonsson2016automated} in this research line had been evaluated on thousands of industrial bug reports. As the pioneers of this research line, Murphy~\emph{et al.}~\cite{murphy2004automatic} analyzed the textual contents of bug reports, and followed-up researchers included more features such as buggy components~\cite{xia2016improving} and bug severity~\cite{xuan2015towards,jonsson2016automated} into their analysis. Interestingly, Zou \emph{et al.}~\cite{zou2011towards} and Xuan \emph{et al.}~\cite{xuan2015towards} showed that fewer features can produce even better results, and Lin~\emph{et al}~\cite{lin2009empirical} showed that the buggy component of a bug report often determines the triage process. Their results reveal a strong need to systematically analyze which features matter in bug assignment, which is the research goal of our study.
%
%
%In recent ten years, researchers have proposed various approaches to assign bug reports. At the beginning of our analysis, we make a simple research (in Section~\ref{sec:knownfeature:paper}) to learn considered features in prior approaches (in Section~\ref{sec:knownfeature:feature}) and used techniques for handling features (in Section~\ref{sec:knownfeature:techniue}) on bug triage.
%
%\subsubsection{Reviewed Papers}
%\label{sec:knownfeature:paper}
%To review prior researches, we query Google Scholar with the keywords such as \textit{bug assignment}, \textit{bug triage}, \textit{change request triage} and \textit{change request assign}. The query returns 109,700 papers, but we cannot read so many papers. As Google returns the most relevant papers on top pages, we check only the top five pages. To locate those useful ones, we define the following three criteria:
%
%\textbf{Criterion 1.} \textit{The research topic must be relevant.} For each paper, we read its title and abstract to determine whether its topic is relevant. We filter those papers, whose titles and abstracts do not have the keywords such as ``assignment'', ``triage'', ``developer assignment'', or ``categori-''. In total, 97 papers are filtered out.
%
%\textbf{Criterion 2.} \textit{Papers must be published in reputable conferences or journals.} In our study, we limit our search scope within the reputable conferences such as \textit{FSE/ESEC, ICSE, ASE, AAAI, ICPC, ICSME, ESEM, SEKE, SANER, WCRE, CSMR, PROMISE and MSR}, and journals such as \textit{TOSEM, TSE, EMSE, TKDE, IST, JSS and SPE.}. We filter out 49 papers.
%
%
%\textbf{Criterion 3.} \textit{Papers must be regular papers.} For conference papers, we select only papers from regular research tracks. We discard 18 records of short papers.
%
%In total, 18 papers published from 2004 to 2019 satisfy all our criteria. For each paper, following the snowballing process guidelines~\cite{DBLP:conf/ease/Wohlin14}, we read its references to locate more relevant papers through Google scholar. We repeat the process until no new papers satisfy all our criteria. In this process, we add additional 13 papers.

%In the literature, the problem of assigning bug reports is typically reduced to a classification problem, and researchers have used various techniques to handle the problem. For example, Cubranic and Murphy~\cite{murphy2004automatic} use Naive Bayes (NB), Anvik \emph{et al.}~\cite{anvik2006should,anvik2006automating} use Support Vector Machine (SVM), C4.5, and Expectation Maximization (EM)~\cite{anvik2007assisting}. Lin \emph{et al.}~\cite{lin2009empirical} also leverage SVM and C4.5, considering both textual and non-textual information. Jonsson et al.~\cite{jonsson2016automated} propose an ensemble model, which combines several known approaches.
%
%The above approaches extract features from bug reports, and use assigned programmers as labels of their classification models. These approaches ignore the expertise of programmers nor their relationships. To handle the problem, Information Retrieval (IR) based models and recommendation-based models are proposed to match the expertises of developers and the requirements of a report. Jeong \emph{et al.}~\cite{jeong2009improving} used Markov Chain to construct tossing graphs to depict the relationships of developers with reassignments considered. Based on tossing graphs, Bhattacharya \emph{et al.}~\cite{bhattacharya2010fine,bhattacharya2012automated} built tossing graphs to denote the reassignments of bug reports, and introduced an ablative analysis on such graphs to improve bug triage. Naguib \emph{et al.}~\cite{naguib2013bug} made recommendations by matching the topics of reports to the expertise of developers mined from activities profiles. Information Retrieval based models, (\emph{e.g.}, Kagdi \emph{et al}~\cite{kagdi2012assigning}) calculated the similarity of the newly reported bug with historical reports with source code entities and then recommended competent developers to solve it.

In our study, we focus on the research line that reduces the bug assignment to a classification problem, since it is intensively studied and a recent approach~\cite{jonsson2016automated} in this research line had been evaluated on thousands of industrial bug reports. As the pioneers of this research line, Murphy~\emph{et al.}~\cite{murphy2004automatic} analyzed the textual contents of bug reports, and followed-up researchers included more features such as buggy components~\cite{xia2016improving} and bug severity~\cite{xuan2015towards,jonsson2016automated} into their analysis. Interestingly, Zou \emph{et al.}~\cite{zou2011towards} and Xuan \emph{et al.}~\cite{xuan2015towards} showed that fewer features can produce even better results, and Lin~\emph{et al}~\cite{lin2009empirical} showed that the buggy component of a bug report often determines the triage process. Their results reveal a strong need to systematically analyze whether textual contents are impactive and which features matter in bug assignment, which is the research goal of our study. To meet the timely need, we analyze the features and the techniques of the prior studies.

\paragraph{Considered Features}
%\label{sec:knownfeature:feature}


\begin{table*}
\caption{Collection of features taken into consideration in previous works}\vspace*{-2ex}
\centering
\scriptsize
\begin{tabular}{|r|p{0.1cm}|p{0.1cm}|p{0.1cm}|p{0.1cm}|p{0.1cm}|p{0.1cm}|p{0.1cm}|p{0.1cm}|p{0.1cm}|p{0.1cm}|p{0.1cm}|p{0.1cm}|p{0.1cm}|p{0.1cm}|p{0.1cm}|p{0.1cm}|}
\hline
\multicolumn{1}{|c|}{\multirow{8}{*}{\textbf{Paper}}} & \multicolumn{16}{c|}{\textbf{Features}} \\
\cline{2-17}
& \multicolumn{3}{c|}{\shortstack{Textual\\content}} & \multicolumn{9}{c|}{\shortstack{Nominal item}} & \multicolumn{4}{c|}{\shortstack{Revision\\histories}} \\
\cline{2-17}
& \rotatebox{90}{titles} & \rotatebox{90}{description} & \rotatebox{90}{comments} & \rotatebox{90}{component} & \rotatebox{90}{project} & \rotatebox{90}{platform} & \rotatebox{90}{version} & \rotatebox{90}{severity} & \rotatebox{90}{reporter} & \rotatebox{90}{commenter} & \rotatebox{90}{re-assignee} & \rotatebox{90}{site} & \rotatebox{90}{commit} & \rotatebox{90}{author} & \rotatebox{90}{maintainer} & \rotatebox{90}{interaction} \\
\hline\hline
% 0.2
Anvik \emph{et al.} (2006)~\cite{anvik2006should} & $\surd$ & $\surd$ & & & & & & & & & & & & & & \\
\hline
% 0.3
Jeong \emph{et al.} (2009)~\cite{jeong2009improving} & & & & & & & & & & & $\surd$ & & & & & \\
\hline
Matter \emph{et al.} (2009)~\cite{matter2009assigning} & $\surd$ & & & & & & & & & & & & & & & \\
\hline
% 0.10
Bhattacharya \emph{et al.} (2010)~\cite{bhattacharya2010fine} & $\surd$ & $\surd$ & & $\surd$ & $\surd$ & & & & & & & & & & & \\
\hline
% 0.6
Park \emph{et al.} (2011)~\cite{park2011costriage} & & $\surd$ & & & & $\surd$ & $\surd$ & & & & & & & & & \\
\hline
% 0.7
Tamrawi \emph{et al.} (2011)~\cite{tamrawi2011fuzzy} & $\surd$ & $\surd$ & & & & & & & & & & & & & & \\
\hline
% 0.8
Anvik \emph{et al.} (2011)~\cite{anvik2011reducing} & $\surd$ & $\surd$ & & & & & & & & & & & & & & \\
\hline
Linares \emph{et al.} (2012)~\cite{linares2012triaging} & $\surd$ & $\surd$ & $\surd$ & & & & & & & & & & & $\surd$ &&\\
\hline
Kagdi \emph{et al.} (2012)~\cite{kagdi2012assigning} & & $\surd$ & $\surd$ & & & & & & & & $\surd$ & & & $\surd$ && \\
\hline
% 0.20
Bhattacharya \emph{et al.} (2012)~\cite{bhattacharya2012automated} & $\surd$ & $\surd$ & & $\surd$ & $\surd$ & & & & & $\surd$ & & & & & & \\
\hline
Hosseini \emph{et al.} (2012)~\cite{hosseini2012market} & & & $\surd$ & $\surd$ & & $\surd$ & $\surd$ & $\surd$ & & & & & & & & \\
\hline
Xie \emph{et al.} (2012)~\cite{xie2012dretom} & $\surd$ & $\surd$ & & & & & & & & & & & & & & \\
\hline
Xia \emph{et al.} (2013)~\cite{xia2013accurate} & $\surd$ & $\surd$ & & $\surd$ & $\surd$ & & & & & & & & & & & \\
\hline
% 0.12
Shokripour \emph{et al.} (2013)~\cite{shokripour2013so} & $\surd$ & $\surd$&  & & & & & & & & & & $\surd$ & & & \\
\hline
% 0.13
Naguib \emph{et al.} (2013)~\cite{naguib2013bug} & $\surd$ & $\surd$ & & $\surd$ & & & & & & & & & & & & \\
\hline
% 1.8
Wang \emph{et al.} (2013)~\cite{wang2013devnet} & & & $\surd$ & $\surd$ & $\surd$ & & & & & $\surd$ & & & & & & \\
\hline
% 0.17
Wang \emph{et al.} (2014)~\cite{wang2014fixercache} & $\surd$ & $\surd$ & & & & & & & & & & & & & & \\
\hline
% 0.26
Hu \emph{et al.} (2014)~\cite{hu2014effective} & $\surd$ & $\surd$ & & $\surd$ & & & & & & & & & & & & \\
\hline
Hossen \emph{et al.} (2014)~\cite{hossen2014amalgamating} & $\surd$ & & & & & & & & & & & & $\surd$ & $\surd$ & $\surd$ & \\
\hline
% 0.18
Xuan \emph{et al.} (2015)~\cite{xuan2015towards} & $\surd$ & $\surd$ & & $\surd$ & $\surd$ & & & $\surd$ & & & & & & & & \\
\hline
% 0.19
Shokripour \emph{et al.} (2015)~\cite{shokripour2015time} & & & & & & & & & & & & & $\surd$ & & & \\
\hline
Zanjani \emph{et al.} (2015)~\cite{zanjani2015using} & & $\surd$ & & & & & & & & & & & & & & $\surd$ \\
\hline
% 0.14
Jonsson \emph{et al.} (2016)~\cite{jonsson2016automated} & $\surd$ & $\surd$ & & & & & & $\surd$ & $\surd$ & $\surd$ & & $\surd$ & & & & \\
\hline
% 0.15
Zhang \emph{et al.} (2016)~\cite{zhang2016ksap} & $\surd$ & $\surd$ & $\surd$ & $\surd$ & $\surd$ & & & $\surd$ & & $\surd$ & & & & & & \\
\hline
Xia \emph{et al.} (2016)~\cite{xia2016improving} & $\surd$ & $\surd$ & & $\surd$ & $\surd$ & & & & & & & & & & & \\
\hline
Tian \emph{et al.} (2016)~\cite{tian2016learning} & $\surd$ & $\surd$ & & & & & & & & & & & & & & \\
\hline
Cavalcanti \emph{et al.} (2016)~\cite{cavalcanti2016towards} &$\surd$ &$\surd$ & &$\surd$ & & & & $\surd$& &  & & & & & & \\
\hline
Sun \emph{et al.} (2017)~\cite{sun2017enhancing} &$\surd$ & $\surd$& & & & & & & & & & &$\surd$ & & $\surd$& \\
\hline
Banerjee \emph{et al.} (2017)~\cite{banerjee2017automated} & $\surd$&$\surd$ & & & & & & &$\surd$ & & & & & & & \\
\hline
 Sarkar \emph{et al.} (2019)~\cite{Sarkar2019improving} & $\surd$&$\surd$&$\surd$&$\surd$&$\surd$&&$\surd$&$\surd$&$\surd$&$\surd$&&$\surd$&&&&\\
\hline
Sajedi \emph{et al.} (2020)~\cite{sajedi2020vocabulary} & $\surd$ & $\surd$ & & $\surd$ & & & & & & & & & & & & \\
\hline
\end{tabular}
\label{tab: features considered}\vspace*{-2ex}
\end{table*}

Table~\ref{tab: features considered} shows our found features and we use ``$\surd$'' to denote that a paper considers a feature. From prior papers, we identify the following 16 features:

% We extract bug reports from Apache projects. Their bug reports have no sites, and most reports have no re-assignees. As we analyze only bug reports, we ignore the four features from source files. In total, we discard six features.

\textit{1. Title.} A title of a bug report is the sentence which summarizes the bug. For example, the title of Figure~\ref{fig: sample} is ``get with timestamp will return a value if there is a version with an earlier timestamp''. In other papers~\cite{naguib2013bug,yang2014towards}, it is called as ``title''.

\textit{2. Description.} A description of a bug report describes the buggy behavior in natural languages, and some code samples can also be provided. For example, in Figure~\ref{fig: sample}, the description of the bug is ``When an explicit timestamp is specified, ..., but returns value...''.

\textit{3. Comment.} The comments of a bug report are the discussions from developers and users. In discussion, they speculate root causes and discuss repair strategies.

\textit{4. Component.} A component indicates which component is buggy. In Figure~\ref{fig: sample}, the reporter does not know which component is buggy, so the component is marked as ``None''.

\textit{5. Project.} A project is the name of the project. In Figure~\ref{fig: sample}, the project is ``HBase''. Projects are considered as a feature, since some approaches (\emph{e.g.}, \cite{bhattacharya2010fine}) assign bugs reports of multiple projects.

\textit{6. Platform.} A platform is the operating system where a bug is found. Some papers~\cite{park2011costriage,kanwal2012bug} consider it as a feature, since fixing bugs on platforms requires different types of expertise.

\textit{7. Version.} A version denotes the releases where a bug is found. The version of Figure~\ref{fig: sample} includes ``\CodeIn{0.2.0}, \CodeIn{0.2.1}, \CodeIn{0.18.0}''. JIRA defines two types of versions such as a fix version and an affects version. When a bug is reported, fix versions are left blank. As a result, we consider only affects version, and call it version for short.

\textit{8. Priority.} A severity denotes the priority or influence of a bug report. JIRA defines five types of priority such as \textit{Trivial}, \textit{Minor}, \textit{Major}, \textit{Critical}, and \textit{Blocker}. For example, the priority shown in Figure~\ref{fig: sample} is ``Critical''.

\textit{9. Reporter.} A reporter is the person who files a bug report. On JIRA, both users and developers can create a bug report. In Figure~\ref{fig: sample}, the reporter is Jim Kellerman.

\textit{10. Commenter.} The commenters of a bug report include the developers who discuss the bug report. Although a report can comment a report, we remove reporters from commenters, so this feature is independent from reporters. The developers in Figure~\ref{fig: sample} are ``Jonathan Gray'' and ``Jim Kellerman''.

\textit{11. Re-assignee} The re-assignees of a bug report are its prior assigned developers. Some approaches\cite{jeong2009improving,kagdi2012assigning} use this feature to build tossing graphs for new assignments prediction.

\textit{12. Site.} A site of a bug report is the place of companies where the bug report is fixed. According to Jonsson \emph{et al.}~\cite{jonsson2016automated}, a bug can be submitted from different countries, and they call this feature as sites. The issue trackers of Apache projects do not record the site of a bug report.

\textit{13. Commit.} A commit includes the code changes and its message. It is extracted from source code repositories.

\textit{14. Author.} The authors of a source file are the developers who are recorded as its authors in code comments.

\textit{15. Maintainer.} The maintainers of a source code file are the developers who make changes to the source code file. This feature is extracted from source code repositories (\emph{e.g.}, Git servers).

\textit{16. Interaction.} An interaction records the activities of developers on the source files. They are collected though the IDEs of developers.

\paragraph{Techniques to Handle Feature}
%\label{sec:knownfeature:techniue}
We classify the features in Table~\ref{tab: features considered} into three categories such as \CodeIn{textual contents}, \CodeIn{nominal items} and \CodeIn{revision histories}. For the three categories of features, we summarize the techniques to handle the features.

\textit{1. Textual contents.} The analysis process of textual contents is similar to that of natural language documents. The preprocessing of textural contents includes removing stop words~\cite{sajedi2020vocabulary}, stemming~\cite{sajedi2020vocabulary,xia2016improving}, removing punctuation marks~\cite{banerjee2017automated}, converting all uppercase characters to lowercase characters~\cite{banerjee2017automated}, splitting the words such as camel cases and underscores~\cite{sajedi2020vocabulary}, and searching synonyms of the verbs and nouns~\cite{sajedi2020vocabulary}. After that, textual contents are converted to vectors. Anvik \emph{et al.}~\cite{anvik2011reducing} used TF-IDF, and the other researchers~\cite{chen2011approach,tamrawi2011fuzzy,tamrawi2011fuzzy,shokripour2015time} used vector space model (VSM). As our baseline, Jonsson \emph{et al.}~\cite{jonsson2016automated} used VSM to translate textual contents into vectors, and it selects 100 words with the highest TF-IDF values.

\textit{2. Nominal items.} The basic idea of handling nominal items is to translate them into values or one-hot vectors. For example, Yang \emph{et al.}~\cite{yang2014towards} translated the priority of a bug report into a vector, where $\langle$1,0,0,0,0$\rangle$ denotes a ``trivial'' bug. As our baseline approach, Jonsson \emph{et al.}~\cite{jonsson2016automated} regarded the different values of a nominal feature as different category labels.

\textit{3. Revision histories.} Tufano \emph{et al.}~\cite{tufano2016there} show that most commits are not compilable after they are checked out. As most code analysis tools need complete code or compiled code, it is challenging to apply advanced analysis on commits. The analysis on commits is typically simple. For example, Shokripour \emph{et al.}~\cite{shokripour2015time} extract identifiers of classes, methods, fields and parameters from commits with a simple parser.

In summary, we summarize the techniques to handle the features from the prior papers. In particular, we find that textual features (\emph{e.g.}, \textit{description} and \textit{title}) are dominate features. In total, 27 out of 31 approaches mainly consider textual features. 
\begin{table}
\centering
\scriptsize
\caption{Jonsson \emph{et al.}~\cite{jonsson2016automated} and our implementation}
\label{tab: cla_acc_comp}\vspace*{-3ex}
\begin{tabular}{|l | c c c c c c|}
\hline
 & BayesNet & SMO & IBk & KStar & RF & Stacking \\
\hline
Jonsson \emph{et al.}~\cite{jonsson2016automated} & 35\% & 42\%-86\% & 38\%-77\% & 42\%-77\% & 39\%-84\% & 50\%-89\% \\
\hline
Our implementation & 53\%-67\% & 74\%-77\% & 54\%-62\% & 2\%-63\% & 32\%-68\% & 89\%-95\% \\
\hline
\end{tabular}
%\begin{minipage}{\linewidth}
%\begin{tablenotes}
%   \footnotesize
%   \item[1] T: the results in Jonsson \emph{et al.}~\cite{jonsson2016automated}; O: our results.
%\end{tablenotes}
%\end{minipage}
\vspace*{-4ex}
\end{table}
\section{The Impact of Textual Feature}
\label{sec:myth}

%\hao{The styles of Sections 4 and 5 must be consistent. I would suggest that you shall move the RQs to Section 1. }
%\hao{You shall not explain the protocol here.}
% Our review reveals that most approaches consider textual features. We notice that in some prior approaches (\emph{e.g.}, \cite{Sarkar2019improving}), researchers also review papers to understand which features are already considered, but they did not conduct empirical studies to understand the importance of known features.
In this section, to explore the impacts of textual features by solid empirical evidence, we explore 2 research questions: the effectiveness of textual features (Section~\ref{sec:RQ1}) and the underlying reasons (Section~\ref{sec:RQ2}).
% We next introduce our dataset (Sections~\ref{sec:myth:data} and Section~\ref{sec:myth:selfassign}),



\subsection{RQ1.The effectiveness of textual features}
\label{sec:RQ1}
In this research question, to prove the effectiveness of textual features, we make a two stages analysis. First, we explore the overall impacts of textual features. Next, we explore the impacts of each textual feature.

\subsubsection{Protocol}
\label{sec:RQ1:setting}
In this research question, we conduct an approach to assign the bug reports in our dataset (the baseline). After that, we disable all its textural features, so its NLP techniques do not work (No Texts). We record the time consumption (including the time for model training, testing and feature preprocessing) and the classification effectiveness (\emph{i.e.}, precisions, recalls, F-measure and AUC).

As a start of our empirical study, we select the approach from a prior research~\cite{jonsson2016automated} as our baseline. It is a recent approach that achieves promising results. The accuracy for assignment reaches 89\% in industrial contexts. They used WEKA~\cite{hall2009weka}, a mature Machine Learning framework in Java, to implement their approach. As they do not release their tool, we follow their steps, and build our own version also on WEKA.

The approach conducts an ensemble learning method which improves the effectiveness by combining several basic classifiers. According to Jonsson \emph{et al.}~\cite{jonsson2016automated}, these basic classifiers are independently used by many other researchers~\cite{jeong2009improving,anvik2006should,anvik2011reducing,ahsan2009automatic}, and their performances (accuracies) are the top five among 28 basic classifiers. Thus, we select these classifiers independently as other baselines. Jonsson provided more details of the stacked-generation learner for implementation. They selected Logistic regression model as the level-1 classifier to combine the level-0 classifiers, including BayesNet, SMO, IBk, KStart and Random Forest.

% We use 10-fold cross-validation as they did in their paper.

We follow their data preprocessing techniques. As a minor difference, Jonsson \emph{et al.} treat the title and the description as a single textual feature, but we handle them as two to explore their individual impacts. As Jonsson \emph{et al.} do not release their dataset, we conduct the approach on our dataset. Table~\ref{tab: cla_acc_comp} shows the accuracies that are reported in their paper (T), and are achieved by our implementation (O). Still, the results are largely consistent with what were reported in their paper~\cite{jonsson2016automated}.



% We compare the performance with influential features chosen to that of all features and of only textual features (\textit{description} and \textit{bug summary}), which is commonly~\cite{wang2014fixercache, moin2012assisting, xuan2012developer, zou2011towards, tamrawi2011fuzzy, anvik2011reducing} used as shown in Table~\ref{tab: features considered}. We implement these model in python with use of sklearn package.

In addition to the effectiveness of all features, we also explore the impact of each textual feature. To fully understand their impacts, we enumerate the combination of textual features and nominal features. Through this evaluation, we can learn the effectiveness of each textual feature and textual features when they are combined with nominal features.

\subsubsection{Results}
\label{sec:RQ1:result}


\begin{figure}
\centering
\includegraphics[width=0.8\textwidth]{figure/boost.pdf}\vspace*{-3ex}
\caption{The execution time (key vs all features)}
\label{fig: time boost}
\end{figure}

Table~\ref{tab: perf boost} shows the effectiveness of the baseline and no texts. In this table, we highlight the results that are better than the compared ones. We find that except for SMO, no texts leads to better results than selecting all features. In particular, for their meta-classifier, after we disable its textual features, we improve their f-score by 8\%.

As shown in Figure~\ref{fig: time boost}, disabling textual features not only improves the effectiveness, but also reduces the execution time. For the meta-classifier, we reduce its execution time by 89\%. Please note that Figure~\ref{fig: time boost} uses a log scale to denote execution times. Our observations lead to our finding:

\medskip
\noindent
\begin{tabular}{|p{11.5cm}|}
\hline
\textbf{Finding 1.} Compared with the baseline, we find that disabling its textual features improves its effectiveness by 8\%, and reduces its execution time by 89\%. \\
\hline
\end{tabular}
\medskip

In our additional study, Table~\ref{tab:combinedfeature} shows the results of the combinations of textual features. We find that when we use only textual features, the best AUC is 0.667 (the combination of bug summaries and comments), which is lower than the combination of our key features that are identified in the next section (0.853). We try to replace our key features with the best combination of textural contents, and the results show that all the replacements lead to lower AUC values.

The results show that classical NLP techniques are insufficient to handle textual features of bug reports. Comparing the combinations of textual information and nominal features reflecting different meanings, we find that the effectiveness is remarkably improved by combining nominal features. The result shows that interests are the major concern for assignment. However, selecting only our key features still achieves the highest AUC values in our comparison.

In summary, our results show that disabling textual features of the baseline improves its effectiveness, and reduces its execution time. Due to space limit, we put the data of all projects together for analysis, and this setting can potentially blur the difference between projects~\cite{easterbrook2008selecting}. However, the impacts shall be minor for three considerations. First, Table~\ref{tab: dataset for projects} shows that except SHIRO, we select similar numbers of bug reports from each project. As a result, the results are not polluted from a single data source. Second, researchers~\cite{zimmermann2009cross} complained that the effectiveness on the cross-project predictions is reduced. However, as shown in Table~\ref{tab: cla_acc_comp}, even if our setting is a mixture of in and cross projects, under this setting, the effectiveness of the baseline is still similar to and even better than what were reported in their paper~\cite{jonsson2016automated}. Finally, we select the key features for each project in Section~\ref{sec:keyfeature:cross}. Table~\ref{tab: fs for projects} shows that in all the projects, textural features are not selected as the key features. As the result is consistent, we believe that our findings will not change much, even if this study analyzes the data of individual projects.
\begin{table}[t]
\centering
\scriptsize
\caption{The effectiveness (baseline vs no texts)}
\label{tab: perf boost}
\begin{tabular}{|l|l|c c c c|}
\hline
\textbf{Model} & \textbf{Setting} & \textbf{Precision} & \textbf{Recall} & \textbf{F-measure} & \textbf{AUC} \\
\hline\hline
\multirow{2}{*}{BayesNet} & Baseline & 0.533 & 0.485 & 0.508 & 0.941 \\
 & No texts & \textbf{0.669} & \textbf{0.669} & \textbf{0.669} & \textbf{0.984} \\
\hline
\multirow{2}{*}{SMO} & Baseline & {0.768} & 0.701 & 0.733 & 0.987 \\
 & No texts & 0.737 & \textbf{0.730} & 0.733 & 0.987 \\
\hline
\multirow{2}{*}{IBk} & Baseline & 0.537 & 0.466 & 0.499 & 0.725 \\
 & No texts & \textbf{0.615} & \textbf{0.607} & \textbf{0.611} & \textbf{0.887} \\
\hline
\multirow{2}{*}{KStar} & Baseline & 0.021 & 0.021 & 0.021 & 0.512 \\
 & No texts & \textbf{0.626} & \textbf{0.618} & \textbf{0.622} & \textbf{0.981} \\
\hline
\multirow{2}{*}{RandomForest} & Baseline & 0.317 & 0.289 & 0.302 & 0.843 \\
 & No texts & \textbf{0.681} & \textbf{0.671} & \textbf{0.676} & \textbf{0.987} \\
\hline
\multirow{2}{*}{Stacking} & Baseline & 0.891 & 0.673 & 0.767 & 0.985 \\
 & No texts & \textbf{0.953} & \textbf{0.725} & \textbf{0.824} & \textbf{0.986} \\
\hline
\end{tabular}
\end{table}

\begin{table}
\centering
\scriptsize
\caption{The combination of features}
\label{tab:combinedfeature}
\begin{tabular}{|p{1.8 cm}|l|c|}
\hline
Items & Features & AUC \\
\hline\hline
\multirow{7}{*}{\shortstack{Textual features}} & Title & 0.539 \\
 & Description & 0.554 \\
 & Comment & 0.645 \\
 & Title + Description & 0.595 \\
 & Title + Comment & 0.667 \\
 & Description + Comment & 0.661 \\
 & Title + Description + Comment & 0.661 \\
\hline
\hline
\multirow{2}{*}{\shortstack{Textual \& key \\features}} & \shortstack{Title + Description + Component + Version} & 0.652 \\
 & \shortstack{Title + Description + Developer + Reporter} & 0.778 \\
\hline
\hline
Key features & Developer + Reporter + Version + Component & 0.853 \\
\hline
\end{tabular}
\end{table}


\subsection{RQ2. Underlying reasons}
\label{sec:RQ2}
Textual features are proved to be less effective (Section~\ref{sec:RQ1}). In this research question, we make manual inspections to investigate the underlying reasons.

\subsubsection{Protocol}
\label{sec:RQ2:setting}
To investigate the underlying reasons, we randomly select 10 bug reports that the approach fails to make correct triage for each project. For each bug report, we go through the textual contents to learn whether textual contents provide enough information. Also, we check the textual features in contrast to textual contents to learn whether NLP techniques extract valid information. Finally, we conclude the underlying reasons on the two perspective.

\subsubsection{Results}
\label{sec:RQ2:result}
After some inspections and discussions, we come to two explanations:

\textit{1. The classic textural analysis techniques are insufficient to handle bug reports.} Bug reports are not written in pure natural language documents, and they contain structural contents such as code samples, stack traces, and patches. However, as we introduced in Section~\ref{sec:background:state}, all the existing approaches use natural language processing techniques to handle bug reports. These techniques do not understand the true meanings of structural contents, since their structures are quite different from natural languages. Such contents can even become noises to natural language processing techniques. Bettenburg \emph{et al.}~\cite{bettenburg2008extracting} proposed an approach to extract structural contents from bug reports. Furthermore, Sun \emph{et al.}~\cite{sun2010discriminative} showed that structural contents are useful to locate duplicated bug reports. If such structural contents are extracted and properly handled, they can be more useful for an automatic approach.

\textit{2. Some bug reports have no useful descriptions to determine assignments.} Zimmermann \emph{et al.}~\cite{zimmermann2010makes} complained that some bug reports are poorly written. For example, LUCENE-4689~\cite{LUCENE4689} has a title ``Eclipse project name change for 4.1'' and a description ``Just updating the eclipse project name from lucene\_solr\_branch\_4x to lucene\_solr\_4\_1 on the new branch.'' The two sentences are difficult to understand. Even for programmers, it is challenging to determine who shall fix the bug, based on the two sentences. Meanwhile, the reporter says that the bug affects 4.1, and the buggy component is \CodeIn{general/build}, which are more useful to assign the bug report than the textual descriptions.


%This piece of description belonging to issue PDFBOX-2454 is composed all by code without any natural language expression. According to our textual preprocessing methods, we filter out the words not in embedding dictionary Glove like ``lcms'', ``java2d'' and function names ``createNativeTranform'' \emph{et al.}, little information remains.
%
%\textit{This can be easily reproduced using provided example \\
%org.apache.pdfbox.examples.pdmodel.RemoveFirstPage that will delete the last page from document instead the first one.
%I think this bug was introduced within the last 3 months as my older snapshot build worked fine.}
%
%This piece of description belonging to issue PDFBOX-2708 dipicts the phenomenon without clear explanation. We can tell the page deletion goes wrong. However, it is not clear which parts of code are supposed to be fixed not to mention how to find out the developers responsible for.
%
%\textit{Just updating the eclipse project name from lucene\_solr\_branch\_4x to lucene\_solr\_4\_1 on the new branch.
%}
%
%This piece of description belonging to issue LUCENE-4689 points out direction of bug fixing without cause description. We cannot figure out why and where the bug raises.

The two observations lead to our finding:

\medskip
\noindent\begin{tabular}{|p{11.5cm}|}
\hline
\textbf{Finding 2.} Classic NLP techniques alone are insufficient to handle bug reports, which contain structural contents. \\
\hline
\end{tabular}
\medskip

We find that textual features do not have positive contributions to the state-of-the-art bug assignment approach. However, we disagree the superficial conclusion that researchers shall not use NLP in their tools. As we discussed in Section~\ref{sec:road_ahead}, there are a strong need and many opportunities to explore better techniques to handle natural language texts in software engineering. After all, NLP techniques are proposed to handle pure natural language texts, but many software engineering documents are the combination of more elements. As a result, the prior approaches are ineffective to use NLP techniques. In Sections~\ref{sec:road_ahead} and \ref{sec:conclusion}, we discuss more issues.


To provide empirical evidence for forward-looking insights, there are two research paradigms. First, researchers can conduct empirical studies on how programmers assign bug reports. These studies can collect empirical evidence for forward-looking insights, because programmers can use features and techniques that do not appear in the prior approaches. Second, researchers can try more features and recent techniques to locate useful ones. The two research paradigms are quite different from what we did in this paper, so we leave them to the future work of other researchers.


% \subsection{Result}
% \label{sec:myth:result}

% To fully understand the impact of textual features, we conduct an additional study, which enumerates the combination of textual features and our found key features. Table~\ref{tab:combinedfeature} shows the results. We find that when we use only textual features, the best AUC is 0.667 (the combination of bug summaries and comments), which is lower than the combination of our key features that are identified in the next section (0.853). We try to replace our key features with the best combination of textural contents, and the results show that all the replacements lead to lower AUC values.

% The results show that classical NLP techniques are insufficient to handle textual features of bug reports. Comparing the combinations of textual information and nominal features reflecting different meanings, we find that the effectiveness is remarkably improved by combining nominal features. The result shows that interests are the major concern for assignment. However, selecting only our key features still achieves the highest AUC values in our comparison.

% In summary, our results show that disabling textual features of the baseline improves its effectiveness, and reduces its execution time. Due to space limit, we put the data of all projects together for analysis, and this setting can potentially blur the difference between projects~\cite{easterbrook2008selecting}. However, the impacts shall be minor for three considerations. First, Table~\ref{tab: dataset for projects} shows that except SHIRO, we select similar numbers of bug reports from each project. As a result, the results are not polluted from a single data source. Second, researchers~\cite{zimmermann2009cross} complained that the effectiveness on the cross-project predictions is reduced. However, as shown in Table~\ref{tab: cla_acc_comp}, even if our setting is a mixture of in and cross projects, under this setting, the effectiveness of the baseline is still similar to and even better than what were reported in their paper~\cite{jonsson2016automated}. Finally, we select the key features for each project in Section~\ref{sec:keyfeature:cross}. Table~\ref{tab: fs for projects} shows that in all the projects, textural features are not selected as the key features. As the result is consistent, we believe that our findings will not change much, even if this study analyzes the data of individual projects.



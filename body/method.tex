\section{Methodology}
\label{sec:method}

%\hao{Rewrite this section. Change all settings to protocols.}

This section introduces our dataset (Section~\ref{sec:data}), our data clean process (Section~\ref{sec:dataclean}), and our general protocol (Section~\ref{sec:method:protocol}).

\subsection{Dataset}
\label{sec:data}

\begin{table}
\caption{Our dataset}\vspace*{-2ex}
\scriptsize
\centering
\begin{tabular}{|l|r r r|}
\hline
Project & Issues & Bug reports &  Developers \\
\hline\hline
CASSANDRA & 10,000 & 3,897 (2,290) & 434 \\
HBASE & 10,000 & 4,802 (2,265) & 347 \\
LUCENE & 8,653 & 2,872 (1,923) & 70 \\
PDFBOX & 4,441 & 2,443 (2,016) & 16 \\
SHIRO & 659 & 279 (256) & 7 \\
\hline
\hline
Total & 33,753 & 14,293 (8,750) & 863 \\
\hline
\end{tabular}
\label{tab: dataset for projects}
\end{table}


As introduced in Section~\ref{sec:background:triage}, most Apache projects use JIRA as their issue trackers. According to Atlanssian, JIRA has been used by over 75,000 customers from 122 countries. In this study, we focus on projects that use JIRA, and select five projects (\textit{HBASE}~\cite{apache.hbase}, \textit{CASSANDRA}~\cite{apache.cassandra}, \textit{SHIRO}~\cite{apache.shiro}, \textit{PDFBOX}~\cite{apache.pdfbox} and \textit{LUCENE}~\cite{apache.lucene}) as our subjects.
% \textit{CASSANDRA} is a software development kit (SDK) for developing cross-platform rich Internet applications. \textit{HBASE} is an open-source, NoSQL database management system designed to handle large amounts of data. \textit{LUCENE} is a search engine. \textit{PDFBOX} is a library for manipulating PDF files. \textit{SHIRO} is a security framework.
These projects are chosen because they cover several types of software (CASSANDRA and HBASE are database management system/model; PDFBOX and LUCENE are JAVA-libraries for PDF files processing and search engine separately; and SHIRO is a security framework) and are of different sizes (the bug reports are more than 10,000 in CASSANDRA and HBASE, while they are fewer than 1,000 in SHIRO).
Table~\ref{tab: dataset for projects} shows a summary of our dataset. We list the total number of issues, the total number of bug reports, and the number of developers. For large projects as \textit{CASSANDRA} and \textit{HBASE}, we select their latest 10,000 issues, and for the other projects, we extract all their issues.

As introduced in Section~\ref{sec:background:triage}, some bug reports are invalid. To remove superficial bug reports, we select bug reports that are marked as ``Closed'' and ``Fixed''. We also remove bug reports whose assignees or reporters are not human beings (\emph{e.g.}, Adobe JIRA). For each bug report, our tool extracts its details, description, reporter, and assignee, for latter analysis.

\subsection{Data Clean}
\label{sec:dataclean}

\begin{figure}[t]
\centering
\includegraphics[width=0.8\textwidth]{figure/RepDev.pdf}\vspace*{-2ex}
\caption{The proportion of self-assignment}
\label{fig: rep&dev}\vspace*{-3ex}
\end{figure}

We notice that some bug reports can be assigned to their reporters, and call it the self-assignment problem. We calculate the ratios, and Figure~\ref{fig: rep&dev} shows the results. In total, 56\% of bug reports are assigned to their reporters. To understand self-assignments, we randomly inspected ten self-assigned bug reports from each project, and identified three cases:



\textit{1. Developers record their work logs in bug reports (56\%).} In such a bug report, the reporter and the assignee are identical, because the programmer needs to record his/her work logs. For example, \textit{HBASE-1026}~\cite{HBASE1026} was reported by a developer called Jean-Daniel Cryans, and it was assigned and fixed by himself.

\textit{2. Reporters volunteer to contribute (40\%).} In these bug reports, reporters volunteer to fix bugs, and attach patches in their reports. After programmers discuss the patches, they assign the reports to the reporters and accept their patches. For example, \textit{HBASE-5008}~\cite{HBASE5008} was reported by a user called ``gaojinchao''. It contained a patch with a comment, ``I made a patch. Please review''.

\textit{3. Bug report are tossed among developers, but finally are assigned to their reporters (4\%).} Some bug reports are assigned to several developers, before it is finally assigned to its reporter. For example, \textit{HBASE-4608}~\cite{HBASE4608} was reported by ``Li Pi'', and 13 developers discussed the bug. The bug report was assigned to ``Zhihong Yu'', ``Li Pi'', and ``stack'', and finally was assigned to its reporter, ``Li Pi''.

As their assignees are already decided, the first and second types of cases need no assignment techniques. Assigning such bug reports does not need any comprehensive techniques, and it can introduce bias. In total, they account for 96\% of all cases. As a result, we remove all self-assignments from our dataset. In Table~\ref{tab: dataset for projects}, the numbers inside brackets denote bug reports after the filtering.


\subsection{General Protocol}
\label{sec:method:protocol}

Our research purpose is to provide thought-provoking reflections on the importance of textual contents and the key features of bug triage. Our study has two major phases such as revisiting the textual features (Section~\ref{sec:method:protocol:revisit}) and exploring the key features of bug triage (Section~\ref{sec:method:protocol:feature}).

Although it is lengthy, our manuscript cannot cover all the perspectives of bug triage. As our study targets thought-provoking reflections, it does not provide direct empirical evidence for forward-looking insights. However, based on our own experiences, our reflections in Section~\ref{sec:road_ahead} include such insights. For example, we believe that handling code snippets and textual contents separatively~\cite{bacchelli2011extracting} and introducing more advanced learning techniques~\cite{lee2017applying} are useful to make improvements.%\hao{Rewrite the general protocols. Use the following paragraphs.}

\subsubsection{Revisiting Textual Features}
\label{sec:method:protocol:revisit}
%\hao{You shall introduce the major steps. You shall not explain results in the protocol section.}

Section~\ref{sec:background:state} shows that 27 out of 31 prior approaches consider textual features. Although researchers widely used textual features, no one has explored their importance. To complement the prior work, we conduct an empirical study on a state-of-the-art approach to understand the importance of textual features. By turning on/off the textual features in the approach, we learn the effectiveness of textual features on bug triage from data perspective. Although the comparison exhibits the importance of textual features, it cannot tell us why textual features are effective/ineffective. To provide convictive examples of our reflection results, we further go through the textual contents to check whether textual contents provide discriminative information for bug triage (see Section~\ref{sec:myth}).

% Our results are surprising, since we actually increase its effectiveness after we remove textual features. The results highlight the importance of building approaches on solid empirical evidence, since even a straightforward technical choice can lead to unexpected results.


\subsubsection{Exploring Key Features}
\label{sec:method:protocol:feature}
%\hao{You shall introduce the major steps. You shall not explain results in the protocol section.}
% Before exploring key features for bug triage, we conduct a survey on the prior researches to learn all considered features (Section~\ref{sec:background:state}).
After exploring the effectiveness of textual features, we also investigate the key features for bug triage. To understand which known features matter, we conduct another empirical study (Section~\ref{sec:keyfeature}). In this study, we use a feature-selection technique~\cite{kohavi1997wrappers} to identify the key features from known ones. To provide thought-provoking reflections, our selected features must be aligned to known features as shown in Section~\ref{sec:background:state}, even if they are controversial. For example, although commenters can leak true labels, we consider this feature, since the prior approaches (\emph{e.g.}, \cite{wang2013devnet}) used this feature. %Since the key features for different projects may show diverse patterns, we also discuss these differences according to characters/sizes of these projects.
% Still, when this feature is selected as a key feature, we add a warning in Finding 3.

%\hao{Did you update this section? This manuscript does not have a survey anymore. }



% We find that textual features do not have positive contributions to the state-of-the-art bug assignment approach. However, we disagree the superficial conclusion that researchers shall not use NLP in their tools. As we discussed in Section~\ref{sec:road_ahead}, there are a strong need and many opportunities to explore better techniques to handle natural language texts in software engineering. After all, NLP techniques are proposed to handle pure natural language texts, but many software engineering documents are the combination of more elements. As a result, the prior approaches are ineffective to use NLP techniques. In Sections~\ref{sec:road_ahead} and \ref{sec:conclusion}, we discuss more issues.

% To provide empirical evidence for forward-looking insights, there are two research paradigms. First, researchers can conduct empirical studies on how programmers assign bug reports. These studies can collect empirical evidence for forward-looking insights, because programmers can use features and techniques that do not appear in the prior approaches. Second, researchers can try more features and recent techniques to locate useful ones. The two research paradigms are quite different from what we did in this paper, so we leave them to the future work of other researchers.



